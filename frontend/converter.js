// Function to convert JSON to CSV
function jsonToCsv(jsonData) {
    function flattenObject(obj) {
        const result = {};
        for (const key in obj) {
            if (typeof obj[key] === 'object' && !Array.isArray(obj[key])) {
                const flattened = flattenObject(obj[key]);
                for (const subKey in flattened) {
                    result[key + '.' + subKey] = flattened[subKey];
                }
            } else {
                result[key] = obj[key];
            }
        }
        return result;
    }

    let csvData = '';

    if (Array.isArray(jsonData)) {
        if (jsonData.length === 0) {
            return 'No data to convert';
        }

        // Extract column headers from the first object in the JSON array
        const flattenedHeaders = Object.keys(flattenObject(jsonData[0]));
        csvData += flattenedHeaders.join(',') + '\n';

        // Convert JSON data to CSV
        jsonData.forEach((item) => {
            const flattenedItem = flattenObject(item);
            const row = flattenedHeaders.map(header => flattenedItem[header]);
            csvData += row.join(',') + '\n';
        });
    } else if (typeof jsonData === 'object') {
        // Handle single JSON object
        const flattenedData = flattenObject(jsonData);
        const headers = Object.keys(flattenedData);
        csvData += headers.join(',') + '\n';
        const row = headers.map(header => flattenedData[header]);
        csvData += row.join(',') + '\n';
    } else {
        return 'Invalid JSON input';
    }

    return csvData;
}

// Function to handle the conversion and display or download CSV
function convertToCsv() {
    const jsonInput = document.getElementById('jsonInput').value.trim();
    
    if (jsonInput === '') {
        console.error('Input is empty.');
        return;
    }

    try {
        const jsonData = JSON.parse(jsonInput);

        // Convert JSON to CSV
        const csvData = jsonToCsv(jsonData);

        if (csvData === 'No data to convert' || csvData === 'Invalid JSON input') {
            console.error(csvData);
            return;
        }

        // Display CSV
        const csvOutput = document.getElementById('csvOutput');
        csvOutput.textContent = csvData;

        // Make download link visible
        const downloadLink = document.getElementById('downloadLink');
        downloadLink.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvData);
        downloadLink.style.display = 'block';
    } catch (error) {
        console.error('Invalid JSON input:', error);
    }
}

// Attach the conversion function to the button click event
const convertButton = document.getElementById('convertButton');
convertButton.addEventListener('click', convertToCsv);
